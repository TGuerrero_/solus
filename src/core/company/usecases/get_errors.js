import { addressValidator, companyValidator } from "../model/errors";

export default class GetCompanyErrors {
  find(objects) {
    const errors = [];
    const knownNames = {};
    let knownCities = {};

    objects.forEach((obj, index) => {
      this.countCities(obj, index, knownCities);

      const lineErrors = companyValidator(obj, knownNames);

      if (lineErrors.length > 0) {
        errors.push({
          id: obj.name,
          line: index + 2,
          errors: lineErrors,
        });
      }
    });
    const addressErrors = addressValidator(knownCities);

    return errors.concat(addressErrors);
  }

  countCities(obj, index, knownCities) {
    obj.address.city.forEach((el) => {
      let lowerCase = el.toLowerCase();
      if (knownCities[lowerCase]) {
        knownCities[lowerCase].line.push(index + 2);
      } else {
        knownCities[lowerCase] = {
          name: lowerCase,
          line: [index + 2],
        };
      }
    });
  }
}
