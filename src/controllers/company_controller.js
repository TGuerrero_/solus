import GetCompanyData from "@core/company/usecases/get_data";
import getCompanyErrors from "@core/company/usecases/get_errors";
import FilterCompanyData from "@core/company/usecases/filter_data";

import CompanyBuilder from "@adapters/builders/company_builder";
import fetchMechanism from "@adapters/fetch_data";
import SearchMechanism from "@adapters/search";

export default class CompanyController {
  async getData(env) {
    this.generator =
      this.generator || new GetCompanyData(CompanyBuilder, fetchMechanism);
    const companies = await this.generator.run(env);
    return companies;
  }

  getErrors(companies) {
    this.errorsGenerator = this.errorsGenerator || new getCompanyErrors();
    return this.errorsGenerator.find(companies);
  }

  async filterData(searchTerm, filters, companies) {
    this.filter = this.filter || new FilterCompanyData(SearchMechanism);
    return await this.filter.run(companies, searchTerm, filters);
  }
}
