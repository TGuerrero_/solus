const Fuse = require("fuse.js");

const generateOption = (keys, threshold = 0) => ({
  ignoreLocation: true,
  findAllMatches: true,
  shouldSort: true,
  tokenize: true,
  matchAllTokens: true,
  maxPatternLength: 32,
  minMatchCharLength: 2,
  threshold,
  keys,
});

function search(term, items, keys, threshold) {
  const fuse = new Fuse(items, generateOption(keys, threshold));

  return fuse.search(term);
}

export default (searchTerm, items, keys, threshold = 0) => {
  if (!searchTerm) {
    return undefined;
  }

  return search(searchTerm, items, keys, threshold);
};
