import CompanyController from "@controllers/company_controller";

export default (_, inject) => {
  inject("companyController", new CompanyController());
};
