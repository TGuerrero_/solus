export default class GetCompanyData {
  constructor(builder, fetchMechanism) {
    this.builder = new builder();
    this.fetchMechanism = fetchMechanism;
  }

  async run(env) {
    const { sheetsAPIKey } = env;

    const values = await this.fetchMechanism(
      sheetsAPIKey,
      "14uwSMZee-CoIJyIpcEf4t17z6eYN-ElYgw_O7dtU5Ok",
      "EMPRESAS"
    );
    if (values == undefined) return [];
    const companies = values
      .slice(1)
      .map((row, i) => {
        let company;
        try {
          company = this.builder.build(row);
        } catch (e) {
          console.log(`[Company Exception] failed for row ${i + 2}`);
          company = null;
        }

        return company;
      })
      .filter((c) => c !== null);

    return companies;
  }
}
