import Company from "../model/company";
import matchesCompanyFilter from "../model/filter";
import { removeAccent } from "@utils/format";
export default class FilterCompanyData {
  constructor(searchMechanism) {
    this.searchMechanism = searchMechanism;
  }

  async run(items, searchTerm, filters) {
    if (filters) items = this.buttonsFilter(items, filters);

    if (searchTerm.trim()) {
      let results = this.textSearchWithStrictResults(
        items,
        searchTerm,
        Company.keys
      );

      if (!results.length)
        results = this.textSearchWithFlexibleResults(
          items,
          searchTerm,
          Company.keys
        );

      return results;
    } else return items;
  }

  buttonsFilter(items, filters) {
    return items.filter((company) => matchesCompanyFilter(company, filters));
  }

  textSearchWithStrictResults(items, searchTerm, keys) {
    return this.searchMechanism(removeAccent(searchTerm.trim()), items, keys);
  }

  textSearchWithFlexibleResults(items, searchTerm, keys) {
    return this.searchMechanism(
      removeAccent(searchTerm.trim()),
      items,
      keys,
      0.15
    );
  }
}
